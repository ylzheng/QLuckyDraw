import QtQuick 2.0

Item {
    id:container
    signal selectItem(var item)
    signal btnBackClicked

    property var stages : []

    Text {
        id:title
        text: "参与人员相册"
        font.pixelSize: 28
        font.bold: true
        color: "white"
        anchors { top: parent.top;  topMargin: 20; horizontalCenter: parent.horizontalCenter }
        height: 40
    }


    Rectangle {
        id: bg
        anchors{
            top: title.bottom;
            topMargin: 10;
            left: parent.left;
            right: parent.right;
            bottom: parent.bottom;
            bottomMargin: 10
            leftMargin: 10
            rightMargin: 10
        }

        width: parent.width
        height: parent.height

        radius: 5
        color:"#9FFFFFFF"

        GridView {
            id:playerView
            anchors{
                top: parent.top;
                bottom: parent.bottom;
                topMargin: 5;
                bottomMargin: 5
                horizontalCenter: parent.horizontalCenter
            }

            width: Math.floor(parent.width/cellWidth)*cellWidth
            model:playerModel
            cellWidth:144
            cellHeight:144
            clip: true
            delegate: Rectangle{
                height: playerView.cellWidth
                width: playerView.cellHeight
                color: "transparent"
                Column {
                    anchors.horizontalCenter: parent.horizontalCenter
                    Image {
                        id : imgAvatar
                        source: avatarUrl
                        width: 100
                        height: 100
                        fillMode: Image.PreserveAspectFit
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    Text { text: name
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.bold: true
                    }
                    Text { text: depart; anchors.horizontalCenter: parent.horizontalCenter }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        selectItem(modelData);
                    }
                }

            }
        }
    }

    ImageButton {
        id: backButton;
        text: qsTr("返回"); rotation: 3
        anchors{right:parent.right; top:parent.top; topMargin: 5; rightMargin: 5}
        scale: 0.8
        onClicked: {
            btnBackClicked();
        }
    }
}
