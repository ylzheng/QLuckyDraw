import QtQuick 2.0

Item {
    id:container
    signal stageIndexSelected(int stageIndex)
    signal btnBackClicked

    property var stages: []

    Text {
        id:title
        text: "请选择一个奖项开始抽奖"
        font.pixelSize: 28
        font.bold: true
        color: "white"
        anchors { top: parent.top;  topMargin: 20; horizontalCenter: parent.horizontalCenter }
        height: 40
    }

    Rectangle {
        id: bg
        anchors{
            top: title.bottom;
            topMargin: 10;
            left: parent.left;
            right: parent.right;
            bottom: parent.bottom;
            bottomMargin: 10
            leftMargin: 10
            rightMargin: 10
        }
        radius: 5
        color:"#9FFFFFFF"

        ListView {
            id:prizeListView
            anchors{
                top: parent.top;
                left: parent.left;
                right: parent.right;
                bottom: parent.bottom;
                topMargin: 20;
                bottomMargin: 20
                leftMargin: 20
                rightMargin: 20
            }
            model:stages
            implicitWidth: parent.width
            clip: true
            delegate: prizeListViewDelegate
        }
    }

    Component {
        id: prizeListViewDelegate
        Item {
            id: listItem
            height:60
            width: prizeListView.width
            Row {
                spacing: 10
                anchors.horizontalCenter: parent.horizontalCenter
                Text {
                    id:titleField;
                    text:modelData.prize.title
                    font.pointSize: 22
                    font.bold: true
                    verticalAlignment: Text.AlignVCenter
                    anchors.verticalCenter: parent.verticalCenter

                    color: {
                        var stage = stages[index]
                        if(stage && !stage.isCompeleted()) {
                            return "black"
                        }
                        else {
                            return "gray"
                        }
                    }
                }
                Text {
                    id:subTitleField
                    text:modelData.prize.subTitle
                    font.pointSize: 22
                    font.bold: true
                    verticalAlignment: Text.AlignVCenter
                    anchors.verticalCenter: parent.verticalCenter

                    color: {
                        var stage = stages[index]
                        if(stage && !stage.isCompeleted()) {
                            return "black"
                        }
                        else {
                            return "gray"
                        }
                    }
                }
                Text {
                    id:commentField
                    text:"[已产生" +  modelData.winners.length + " / " + modelData.prize.count + "]"
                    font.pointSize: 13
                    font.bold: true
                    verticalAlignment: Text.AlignVCenter
                    anchors.verticalCenter: parent.verticalCenter

                    color: {
                        var stage = stages[index]
                        if(stage && !stage.isCompeleted()) {
                            return "black"
                        }
                        else {
                            return "gray"
                        }
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    var stage = stages[index]
                    if(stage && !stage.isCompeleted()) {
                        stageIndexSelected(index)
                    }
                    else {
                        console.log("Compeleted!");
                    }
                }

                onEntered: {
                    listItem.scale = 1.1
                }

                onExited: {
                    listItem.scale = 1.0
                }
            }
        }
    }

    ImageButton {
        id: backButton;
        text: qsTr("返回"); rotation: 3
        anchors{right:parent.right; top:parent.top; topMargin: 5; rightMargin: 5}
        scale: 0.8
        onClicked: {
            btnBackClicked();
        }
    }

    Component.onCompleted: {
    }
}
