.pragma library

.import "Config.js" as Settings

var currentView_  = {};
var stages_ = [];

function func() {
    //    console.log(Settings.app.meta.appname)
}

function print(player) {

}

function enterView(view) {
    if(currentView_ !== view) {
        currentView_.visible = false;
        view.visible = true;
        currentView_ = view

        currentView_.update()
    }
}

function Stage(stageIndex, prize) {
    this.stageIndex = stageIndex;
    this.prize = prize;
    this.winners = []

    console.log("new Stage: prize.count=", prize.count, "stageIndex=", this.stageIndex)
}

Stage.prototype.win = function(player) {
    if(player.winStatus === 0) {
        player.winStatus = this.stageIndex + 1;
        this.winners.push(player)
        console.log("Player:", player.name, " is winning at stage:", player.winStatus)
    }
    else {
        console.log("Player:", player.name, " was win at stage:", player.winStatus)
    }

    return this.winners.length
}

Stage.prototype.isCompeleted = function() {
    var ok = false;
    if(this.winners.length >= this.prize.count) {
        ok = true;
    }
    return ok;
}

Stage.prototype.nextGroupWinnerCount = function() {
    var count = 0;

    if (this.winners.length < this.prize.count) {
        count = this.prize.count - this.winners.length;
        var each = this.prize.capacity;
        if(count > 0 && each  > 0) {
            count = Math.floor(Math.min(count, each));
        }
    }

    return count;
}

Stage.prototype.isCompleted = function() {
    return ( this.prize.count === this.winners.length);
}

function loadStages() {
    var prizes = Settings.app.config.prizes;
    stages_ = [];
    for(var i in Settings.app.config.prizes) {
        var stage = new Stage(i, prizes[i]);
        stages_.push(stage)
    }
}
