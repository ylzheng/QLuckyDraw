var app = {
    meta:{
        // 版权信息,请勿修改
        appname:"QLuckyDraw",
        version:"v1.0.0",
        author:"Xinus Wang <xinus.wang#gmail.com>",
        copyright:"(C)2013-2014 Xinus Wang. All rights reserved.",
        license:"MIT License"
    },

    title:"幸运大抽奖", // 程序标题
    background: "images/bg-3.jpg",
    music: {
        on: true, // 是否开启背景音乐: true | false
        source: "musics/musicOfBackground.mp3", // 背景音乐文件
        volume:0.3, // 音量 0.1 - 1.0
    },

    config: {
        title:qsTr("XXXX公司2013-2014新春年会"),
        titleColor:"white",
        subTitle:"幸运抽奖",
        subTitleColor:"white",
        prizes:[
            // 奖项设置，抽奖的顺序与此处定义一致
            {
                title:"幸运大奖",
                subTitle:"现金500RMB",
                count:60, // 奖品数量
                capacity:10, // 一次最多抽几个, [1, count)
            },
            {
                title:"三等奖",
                subTitle:"三星手机1台",
                count:10,
                capacity:5, // 一次最多抽几个, [1, count)
            },
            {
                title:"二等奖",
                subTitle:"三星笔记本电脑1台",
                count:5,
                capacity:1, // 一次最多抽几个, [1, count)
            },
            {
                title:"一等奖",
                subTitle:"奥迪A6一辆",
                count:3,
                capacity:1, // 一次最多抽几个, [1, count)
            },
        ],
    },
}
