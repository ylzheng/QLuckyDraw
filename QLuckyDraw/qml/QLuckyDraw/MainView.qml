import QtQuick 2.0
import "script/Config.js" as Settings
Item{
    signal btnStartClicked
    signal btnPlayersClicked
    signal btnPrizesClicked
    signal btnSettingsClicked
    signal btnExitClicked

    id: container

    Text {
        id:labTitle
        text: Settings.app.config.title
        font.pixelSize: 32
        font.bold: true
        color: Settings.app.config.titleColor
        anchors { top: parent.top;  topMargin: 30; horizontalCenter: parent.horizontalCenter }
    }

    Text {
        id:labSubTitle
        text: Settings.app.config.subTitle
        font.pixelSize: 24
        //font.italic: true
        color: Settings.app.config.subTitleColor
        anchors { top: labTitle.bottom;  topMargin: 10; horizontalCenter: parent.horizontalCenter }
    }

    Row {
        spacing: 30; anchors { bottom: parent.bottom;  bottomMargin: 20; horizontalCenter: parent.horizontalCenter }

        ImageButton {
            id: playersButton;
            text: qsTr("参与人员"); rotation: 3
            anchors.verticalCenter: parent.verticalCenter
            onClicked: {
                btnPlayersClicked();
            }
        }

        ImageButton {
            id: prizesButton;
            text: qsTr("中奖名单"); rotation: -2
            anchors.verticalCenter: parent.verticalCenter
            onClicked: {
                btnPrizesClicked();
            }
        }

        ImageButton {
            id: startButton; text: qsTr("开始"); rotation: 3;
            scale:1
            onClicked: {
                btnStartClicked();
            }
            anchors.verticalCenter: parent.verticalCenter
        }

        ImageButton {
            id: settingsButton;
            text: qsTr("设置"); rotation: -2
            anchors.verticalCenter: parent.verticalCenter
            onClicked: {
                btnSettingsClicked();
            }
        }

        ImageButton {
            id: quitButton; text: qsTr("退出"); rotation: 3;
            onClicked: {
                btnExitClicked();
            }
            anchors.verticalCenter: parent.verticalCenter
        }
    }
}
