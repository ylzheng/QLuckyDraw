import QtQuick 2.2
import QtMultimedia 5.0
import QtQuick.Controls 1.0
import QtQuick.Dialogs 1.1

import "script/Engine.js" as Engine
import "script/Config.js" as Settings

ApplicationWindow {
    title: Settings.app.title
    width: 1136
    height: 640
    id:mainWindow

    Image {
        id: bg
        source: Settings.app.background
        fillMode: Image.PreserveAspectCrop
        anchors.fill: parent
        antialiasing:true
    }

    MainView {
        id:mainView
        visible: false
        anchors.fill: parent

        onBtnExitClicked: {
            dialogQuit.open()
        }

        onBtnPlayersClicked: {
            playersView.stages = Engine.stages_
            Engine.enterView(playersView)
        }

        onBtnPrizesClicked: {
            prizeListView.stages = Engine.stages_
            Engine.enterView(prizeListView)
        }

        onBtnStartClicked: {
            prizeMenuView.stages = Engine.stages_
            Engine.enterView(prizeMenuView)
        }
    }

    PlayersView{
        id:playersView
        visible: false
        anchors.fill: parent
        onSelectItem: {
            console.log(item.name, "ss")
        }

        onBtnBackClicked: {
            Engine.enterView(mainView)
        }
    }

    LuckyDrawView {
        id:luckyDrawView
        anchors.fill: parent
        Component.onCompleted: visible=false

        onBtnBackClicked: {
            prizeMenuView.stages = Engine.stages_
            Engine.enterView(prizeMenuView)
        }

        onRollingChanged: {
            if(rolling) {
                playMusic.volume = playMusic.volume * 0.5;
            }
            else {
                playMusic.volume = playMusic.volume * 2;
            }
        }
    }

    PrizeListView {
        id:prizeListView
        anchors.fill: parent
        Component.onCompleted: visible = false

        onBtnBackClicked: {
            Engine.enterView(mainView)
        }

        onStageIndexSelected: {
            if(stageIndex < Engine.stages_.length) {
                winnerListView.stage = Engine.stages_[stageIndex]
                console.log("enterView winnerListView")
                Engine.enterView(winnerListView)
            }
            else {
                console.error("stageIndex out of range.")
            }
        }
    }

    PrizeMenuView {
        id:prizeMenuView
        anchors.fill: parent
        stages:Engine.stages_

        Component.onCompleted: visible = false

        onBtnBackClicked: {
            Engine.enterView(mainView)
        }

        onStageIndexSelected: {
            if(stageIndex < Engine.stages_.length) {
                luckyDrawView.stage = Engine.stages_[stageIndex]
                console.log("enterView luckyDrawView")
                Engine.enterView(luckyDrawView)
            }
            else {
                console.error("stageIndex out of range.")
            }
        }
    }

    WinnerListView {
        id: winnerListView
        anchors.fill: parent
        Component.onCompleted: visible = false

        onBtnBackClicked: {
            prizeListView.stages = Engine.stages_
            Engine.enterView(prizeListView)
        }
    }

    MessageDialog {
        id:dialogQuit
        icon: StandardIcon.Question
        text: "退出程序将无法保存当前中奖信息"
        title: "提示"
        standardButtons: StandardButton.Ok | StandardButton.Cancel

        onAccepted: {
            Qt.quit();
        }

        Component.onCompleted: visible = false
    }

    Audio {
        id: playMusic
        source: Settings.app.music.source
    }

    Component.onCompleted: {
        Engine.loadStages();
        playersView.stages = Engine.stages_;

        if (Settings.app.music.on) {
            //载入完成后播放音乐
            playMusic.play();
            playMusic.volume = Settings.app.music.volume
        }

        Engine.enterView(mainView)
    }
}
