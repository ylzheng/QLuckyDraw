import QtQuick 2.0

Item {
    id:container
    signal btnBackClicked

    property var stage

    Text {
        id:theTitle
        text: "中奖名单"
        font.pixelSize: 28
        font.bold: true
        color: "white"
        anchors { top: parent.top;  topMargin: 20; horizontalCenter: parent.horizontalCenter }
        height: 40
    }

    ImageButton {
        id: backButton;
        text: qsTr("返回"); rotation: 3
        anchors{right:parent.right; top:parent.top; topMargin: 5; rightMargin: 5}
        scale: 0.8
        onClicked: {
            btnBackClicked();
        }
    }

    Rectangle {
        id: bg
        anchors{
            top: theTitle.bottom;
            topMargin: 10;
            left: parent.left;
            right: parent.right;
            bottom: parent.bottom;
            bottomMargin: 10
            leftMargin: 10
            rightMargin: 10
        }

        width: parent.width
        height: parent.height

        radius: 5
        color:"#9FFFFFFF"

        Text {
            id:title
            font.pointSize: 24
            font.bold: true
            color:"steelblue"
            horizontalAlignment:Text.AlignHCenter
            anchors {
                horizontalCenter: parent.horizontalCenter
                top:parent.top
                topMargin: 5
            }
        }

        Text {
            id:subTitle
            font.pointSize: 16
            font.bold: true
            color:"steelblue"
            horizontalAlignment:Text.AlignHCenter
            anchors {
                horizontalCenter: parent.horizontalCenter
                top:title.bottom
                topMargin: 5
            }
        }

        GridView {
            id:playerView
            anchors{
                top: subTitle.bottom;
                bottom: parent.bottom;
                topMargin: 10;
                bottomMargin: 10
                horizontalCenter: parent.horizontalCenter
            }

            implicitWidth: parent.width
            model:rollingModel
            cellWidth:144
            cellHeight:144
            clip: true
            delegate: Rectangle{
                height: playerView.cellWidth
                width: playerView.cellHeight
                color: "transparent"
                Column {
                    anchors.horizontalCenter: parent.horizontalCenter
                    Image {
                        source: avatarUrl
                        width: 100
                        height: 100
                        fillMode: Image.PreserveAspectFit
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    Text {
                        text: name;
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    Text {
                        text: depart;
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                    }
                }

            }
        }
    }

    ListModel {
        id: rollingModel
    }

    Image {
        id:stubImage
        source:"images/attendee_default.png"
        visible: false
    }

    onStageChanged: {
        initStage()
    }

    function initStage() {

        if(rollingModel.count > 0)
            rollingModel.remove(0, rollingModel.count)

        if(stage) {
            console.log("using Stage: prize.count=", stage.prize.count, ", winners.length=",stage.winners.length)

            title.text = stage.prize.title + " " + stage.prize.subTitle
            updateWinners();
        }
        else {
            title.text = nil
            subTitle.text = nil
            btnStart.visible = false;
        }


    }

    function updateWinners() {
        //subTitle.text = "共" + (stage.winners.length)+"名"

        var initGroupCount = stage.winners.length;

        for(var i in stage.winners) {
            var player = stage.winners[i]
            rollingModel.append({name:player.name, depart:player.depart, avatarUrl:Qt.resolvedUrl(player.avatarUrl)})
        }

        var maxCellInRow =  (initGroupCount < 5 ? initGroupCount : 5)
        playerView.width =  Math.min(bg.width, playerView.cellWidth * maxCellInRow);
    }
}
