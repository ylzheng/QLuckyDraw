#ifndef CSPLAYERITEM_H
#define CSPLAYERITEM_H

#include <QObject>
#include <QUrl>

class CSPlayerItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString depart READ depart WRITE setDepart NOTIFY departChanged)
    Q_PROPERTY(QUrl avatarUrl READ avatarUrl WRITE setAvatarUrl NOTIFY avatarUrlChanged)
    Q_PROPERTY(int winStatus READ winStatus WRITE setWinStatus NOTIFY winStatusChanged)
public:
    explicit CSPlayerItem(QObject *parent = 0);
    CSPlayerItem(const QString& name, const QString& depart, const QUrl& avatarUrl, QObject* parent = 0);

    QString name();
    void setName(const QString& name);

    QString depart();
    void setDepart(const QString& depart);

    QUrl avatarUrl();
    void setAvatarUrl(const QUrl& url);

    int winStatus();
    void setWinStatus(int winStatus);

signals:
    void nameChanged();
    void departChanged();
    void avatarUrlChanged();
    void winStatusChanged();

public slots:

private:
    QString name_;
    QString depart_;
    QUrl avatarUrl_;
    int winStatus_;
};

#endif // CSPLAYERITEM_H
