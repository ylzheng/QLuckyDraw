#include "csplayeritem.h"

CSPlayerItem::CSPlayerItem(QObject *parent) :
    QObject(parent)
{
}

CSPlayerItem::CSPlayerItem(const QString &name, const QString &depart, const QUrl &avatarUrl, QObject *parent) :
    QObject(parent), name_(name), depart_(depart), avatarUrl_(avatarUrl), winStatus_(0)
{
}

QString CSPlayerItem::name()
{
    return name_;
}

void CSPlayerItem::setName(const QString& name)
{
    if(name != name_)
    {
        name_ = name;
        emit nameChanged();
    }
}

QString CSPlayerItem::depart()
{
    return depart_;
}

void CSPlayerItem::setDepart(const QString& depart)
{
    if(depart_ != depart)
    {
        depart_ = depart;
        emit departChanged();
    }
}

QUrl CSPlayerItem::avatarUrl()
{
    return avatarUrl_;
}

void CSPlayerItem::setAvatarUrl(const QUrl& url)
{
    if(avatarUrl_ != url)
    {
        avatarUrl_ = url;
        emit avatarUrlChanged();
    }
}

int CSPlayerItem::winStatus()
{
    return winStatus_;
}

void CSPlayerItem::setWinStatus(int winStatus)
{
    if(winStatus_ != winStatus)
    {
        winStatus_ = winStatus;
        emit winStatusChanged();
    }
}
