#include "qtquick2controlsapplicationviewer.h"
#include <QTextCodec>
#include <QFont>
#include <QFontDatabase>

int main(int argc, char *argv[])
{
    QTextCodec::setCodecForLocale(QTextCodec::codecForUtfText("utf-8"));
    Application app(argc, argv);

    int fontId = QFontDatabase::addApplicationFont("fonts/wqy-microhei.ttc");
    QString ftFamily = QFontDatabase::applicationFontFamilies(fontId).at(0);
    Application::setFont(QFont(ftFamily));

    QtQuick2ControlsApplicationViewer viewer;
    viewer.setMainQmlFile(QStringLiteral("qml/QLuckyDraw/main.qml"));
    viewer.show();

    return app.exec();
}
