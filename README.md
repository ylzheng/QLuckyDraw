#QLuckyDraw
QLuckyDraw是一个基于Qt开发的抽奖程序，适用于年会抽奖等场景。

#Useages
##Build
从Qt的官方网站的下载Qt5.2的开发环境。工程的运行路径设置成QLuckyDraw_Release目录。
##Configure
###1.players 目录下面放置人员的信息，文件的结构大致如下:
players/[部门一]/员工1.png
players/[部门一]/员工2.png
players/[部门二]/员工3.png
###2.程序的其他配置在 qml/QLuckyDraw/script/Config.js 中，目前可以配置的包括公司名称、奖项等，也可以修改QML文件达到相应目的。

#Screenshots
![screenshot-1](https://git.oschina.net/xinus/QLuckyDraw/raw/master/screenshot-1.png)
![screenshot-1](https://git.oschina.net/xinus/QLuckyDraw/raw/master/screenshot-2.png)

#Venders
##Qt Project <http://qt-project.org/>
Qt is a cross-platform application and UI framework for developers using C++ or QML, a CSS & JavaScript like language.